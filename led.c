#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "led.h"

//allume ou eteind la led n° "gpio" (sens = 1 -> allume / sens = 0 -> eteind)

void led(char * gpio, int sens)
{
	FILE * f;
	char nom_fichier[50];
	
	strcpy(nom_fichier, "/sys/class/gpio/gpio");
	strcat(nom_fichier, gpio);
	strcat(nom_fichier, "/value");
	
	//printf("%s\n", nom_fichier);
	
	f = fopen(nom_fichier, "w");
	if (f)
	{
		fprintf(f, "%d", sens);
		fclose(f);
	}
	else printf("erreur !\n");
}



//initialise les leds : met les gpio en m
void init_led(char * gpio)
{
	FILE * f, * export;
	char nom_fichier[50];
	
	strcpy(nom_fichier, "/sys/class/gpio/gpio");
	strcat(nom_fichier, gpio);
	strcat(nom_fichier, "/direction");
	
	printf("%s\n", nom_fichier);
	
	export = fopen("/sys/class/gpio/export", "w");
	if(export)
	{
		fprintf(export, "%s\n", gpio);	
		fclose(export);
	}
	else printf("erreur export\n");
	
	usleep(150000);
	f = fopen(nom_fichier, "w");
	if (f)
	{	
		fprintf(f, "out");
		fclose(f);
	}	
	else printf("erreur direction!\n");
}

void * thread_clignote(void * ll)
{
	int clignote = 0;
	char * ld;
	ld = (char*)malloc(sizeof(char) * strlen((char *) ll));
	strcpy(ld, (char *) ll);

	while(1)
	{
		led(ld, clignote);
		clignote ++;
		clignote %= 2;
		usleep(150000);
	}			
}

void * ledConfirmation(void * ll)
{
	char * ld;
	ld = (char*)malloc(sizeof(char) * strlen((char *) ll));
	strcpy(ld, (char *) ll);

	led(ld, 1);
	sleep(2);
	led(ld,0);
}

void findeprog(char * jaune, char * verte, char * rouge)
{
	int i;
	
	for(i = 0; i <10 ; i ++)
	{
		led(jaune, 1);	//eteindre la led jaune
		led(verte, 1);	//eteindre la led verte
		led(rouge, 1);	//eteindre la led rouge
		usleep(150000);
		led(jaune, 0);	//eteindre la led jaune
		led(verte, 0);	//eteindre la led verte
		led(rouge, 0);	//eteindre la led rouge
		usleep(150000);
	}
}
