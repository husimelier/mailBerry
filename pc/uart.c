#define _DEFAULT_SOURCE

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "uart.h"

int fin;


int uart_open(char const*filename) {
	int speed = B115200;

	struct termios options;
	int fd = open(filename, O_RDWR|O_NONBLOCK);
	if(fd < 0) return fd;

	fcntl(fd, F_SETFL, 0);
	tcgetattr(fd, &options);
	usleep(10000);
	cfsetospeed(&options, speed);
	cfsetispeed(&options, speed);
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;
	options.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
	options.c_oflag &= ~CRTSCTS;
	options.c_oflag &= ~OPOST;
	options.c_lflag &= ~(ICANON|ECHO|ECHONL|IEXTEN|ISIG);

	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 40;

	tcsetattr(fd, TCSANOW, &options);
	tcflush(fd, TCIOFLUSH);
	usleep(10000);

	return fd;
}



void * thread_read(void * arg) {
	FILE * fileRead;
	char buf[100];
	
	int fd = *(int*)arg;
	
	fileRead = fdopen(fd, "r");
	
	if(fileRead) {
		while(1) {
			fgets(buf, 100, fileRead);
			if(!strcmp(buf, "FIN\n")) {
				fin = 1;
			}
			else printf("%s", buf);
		}
		fclose(fileRead);
	}
}





int main() {
	int fd;
	char buf[100];
	FILE * fileWrite;
	pthread_t th0;
	
	fin = 0;
	
	fd = uart_open("/dev/ttyUSB0");
	fileWrite = fdopen(fd, "w");
	
	pthread_create(&th0, NULL, &thread_read, &fd);

	if(fileWrite) {		
		while(!fin) {
			fgets(buf, 100, stdin);
			fprintf(fileWrite, "%s", buf);
		}
		fclose(fileWrite);
	}
	pthread_cancel(th0);
	close(fd);
}
