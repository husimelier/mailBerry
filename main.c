#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>

#include "serveur.h"
#include "led.h"
#include "uart.h"

int menu(int fd,FILE * flowin, FILE * flowout, char * jaune,char * rouge)
{
	int choix, fin = 0 ;
	char buff[100];

	do
	{	
		fprintf(flowout, "\nENTREZ LE NUMERO ASSOCIE A VOTRE COMMANDE\n0: Envoyer un message\n1: Lire les messages envoyes\n2: Lire les messages envoyes par destinataire\n3: Lire les messages reçus deja lus\n4: Lire les messages recus deja lus par emetteur\n5: Lire les messages recus non lus\n6: Quitter\n");
		fgets(buff, 100, flowin);
		choix = atoi(buff);
		switch(choix)
		{
			case 0 :
				led(rouge, 0);
				Send(fd, flowin, flowout, jaune, rouge);
			break;

			case  1:
				led(rouge, 0);
				ReadMessage("/home/pi/Desktop/mailBerry/fileMsg/messagesend.txt", flowout);
			break;
			case  2:
				led(rouge, 0);
				affichageNom(flowin, flowout, "/home/pi/Desktop/mailBerry/fileMsg/messagesend.txt", 13);
			break;
			case 3:
				led(rouge, 0);
				ReadMessage("/home/pi/Desktop/mailBerry/fileMsg/messagerecv.txt", flowout);
			break;
			case  4:
				led(rouge, 0);
				affichageNom(flowin, flowout, "/home/pi/Desktop/mailBerry/fileMsg/messagerecv.txt", 5);
			break;
			case 5:
				led(rouge, 0);
				fprintf(flowout, "Vos messages non lu :\n");
				ReadAndSaveMessage(flowout);
			break;
			case 6:
				fin = 1;
				fprintf(flowout, "FIN\n");
			break;
			default:
				led(rouge, 1);
				fprintf(flowout, "Commande inconnue\n");
		}

	}while(choix < 0 || choix > 6);

	return fin;
}

int main()
{
	int fd, fin = 0 ;
	FILE * flowout, * flowin;
	pthread_t th0 ;
	Msg msgrec;
	char jaune[] = "13", verte[] = "19", rouge[] = "26";  

	fd = tcp_connect("192.168.102.249", "1337");
	id = msgget(0x1234, IPC_CREAT | 0600);
	vider_queue_msg(id); 
	flowout = fdopen(uart_open("/dev/ttyS0"), "w");
	flowin = fdopen(uart_open("/dev/ttyS0"), "r");
	//flowout = stdout;
	//flowin = stdin;

	init_led(jaune);	//initialise la led gpio2 jaune
	init_led(verte);	//initialise la led gpio3
	init_led(rouge);	//initialise la led gpio4

	led(jaune, 0);	//eteindre la led jaune
	led(verte, 0);	//eteindre la led verte
	led(rouge, 0);	//eteindre la led rouge
	
	if(flowin)
	{
		if(flowout)
		{
			if(fd>=0)
			{
				pthread_create (&th0, NULL, &thread_recv, &fd);
				login(fd, flowin, flowout, jaune, rouge);
				while(!fin)
				{
					fin = menu(fd, flowin, flowout, jaune, rouge);
				}
			}
			close(fd);
			fprintf(flowout, "Vous etes deconnecte\nVos messages non lu :\n");
			ReadAndSaveMessage(flowout);
			fprintf(flowout, "\nAppuyez sur entrer\n");	
			fclose(flowout);
		}
		else
			printf("erreur ouverture uart\n");

		fclose(flowin);
	}
	else
		printf("erreur ouverture uart\n");

	findeprog(jaune, verte, rouge) ;

	return 0;
}
