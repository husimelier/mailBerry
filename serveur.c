#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>

#include "led.h"
#include "serveur.h"

#define ERROR_IF(F) if(F) { perror(#F); exit(errno); }

pthread_t thClignote;
int clignote = 0; 

void  affichageNom(FILE* flowin, FILE * flowout, char * path, int type)
{
	char name[100];
	
	fprintf(flowout,"Entrez le nom de la personne\n");
	fgets(name, 100, flowin);
	name[strlen(name)-1]='\0';
	affiche(flowout, name, path, type);
}

void affiche(FILE * flowout, char * name, char * path, int type)
{
	FILE * fichier = fopen(path, "r");
	char buff[1000];
	int compteur = 0 ;
	
	if(fichier)
	{
		if(type == 5)
			fprintf(flowout,"\nMessage recus de %s:\n\n", name);
		else if (type == 13)
			fprintf(flowout,"\nMessage envoyes a %s:\n\n", name);
			
		while(fgets(buff,1000, fichier))
		{
			if(testName(name, buff, type))
			{
				compteur ++;
				if(type == 5)
					supressionElt(buff, type+strlen(name)+1);
				else if(type == 13)
				{
					fgets(buff,1000, fichier);
					supressionElt(buff, 9);	
				}
				
				fprintf(flowout,"Message %d: %s\n", compteur, buff);
			}
		}
		if(!compteur)
			fprintf(flowout,"Aucun message trouve !\n");
			
		fclose(fichier);
	}
}

int testName(char * name, char * buff, int type)
{
	int taille = strlen(name);
	int i = 0;
	int egale = 1;
	
	while(egale && i < taille)
	{
		if(name[i] != buff[i+type])
		{
			egale = 0;
		}
		
		i ++;
	}
		
	return egale;
}

void supressionElt(char * buff, int type)
{
	int taille = strlen(buff) - type;
	int i;
	
	for (i = 0 ; i < taille ; i ++)
		buff[i] = buff[i+ type];
		
	buff[taille] = '\0';
}

int tcp_connect(char *addr, char *port) {
	struct addrinfo hints, *res;
	int fd;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	ERROR_IF(getaddrinfo(addr, port, &hints, &res) != 0);
	ERROR_IF((fd = socket(res->ai_family, res->ai_socktype, 0)) == -1);
	ERROR_IF(connect(fd, res->ai_addr, res->ai_addrlen) == -1);

	return fd;
}

void login(int fd, FILE * flowin, FILE * flowout, char * jaune, char * rouge)
{
	char username[100], buf[105];
	int test=0, nb_read;
	pthread_t th0 ;
	Msg msg;

	do
	{
		fprintf(flowout,"Bienvenue\n");
		fprintf(flowout, "quel est ton nom (max 100 caracteres) ?: \n");
		fgets(username, 100, flowin);
		strcpy(buf, "nick ");
		strcat(buf, username);
		printf("commande envoyee :%s", buf);
		write(fd, buf, strlen(buf));
		msgrcv(id, &msg, sizeof(Msg), 1, MSG_NOERROR);
		printf("code de message =%c\n",msg.code); 
		switch (msg.code)
		{
			case '0' :
			pthread_create (&th0 , NULL , &ledConfirmation , jaune);
			test = 1;
			fprintf(flowout, "connection etablie\n");
			break;
			case '2' :
			led(rouge, 1);
			fprintf(flowout, "il faut au moins 1 caractere dans ton nom\n");
			break;
			case '3' :
			led(rouge, 1);
			fprintf(flowout, "ce nom est deja pris\n");
			break;
			case '4' :
			led(rouge, 1);
			fprintf(flowout, "les caracteres speciaux ne sont pas acceptes\n");
			break;
			default :
			led(rouge, 1);
			fprintf(flowout, "erreur d'origine inconnue...\n");
			break;
		}

	} while (!test);
}

void SaveMessageSend(char * recipient, char * msg)
{
	FILE * f = fopen("/home/pi/Desktop/mailBerry/fileMsg/messagesend.txt","a+");

	if(f)
	{
		fprintf(f,"Destination: %s\nMessage: %s\n",recipient, msg);
		fclose(f);
	}
}

void SaveMessageRecv(char * msg)
{
	FILE * f = fopen("/home/pi/Desktop/mailBerry/fileMsg/messagerecvnoread.txt", "a+");
	printf("message a enregistrer : %s\n", msg);
	if(f)
	{
		fprintf(f, "%s\n", msg);
		fclose(f);
	}
	else printf("pb d'ouverture\n");
}

void ReadMessage(char* path, FILE * flowout)
{
	FILE * f = fopen(path,"r");
	char buff[1000];
	
	if(f)
	{
		while(fgets(buff, 1000, f))
		{
			fprintf(flowout,"%s", buff);
		}
		fclose(f);
	}
}

void RemoveFIle(char* path)
{
	FILE * f = fopen(path,"w");
	
	if(f)
	{
		fclose(f);
	}
}

void ReadAndSaveMessage(FILE * flowout)
{
	FILE * read, * noread ;
	char buff[1000];

	noread = fopen("/home/pi/Desktop/mailBerry/fileMsg/messagerecvnoread.txt","r");
	read = fopen("/home/pi/Desktop/mailBerry/fileMsg/messagerecv.txt","a+");

	if(noread)
	{
		if(read)
		{
			while(fgets(buff, 1000, noread))
			{	
				fprintf(flowout,"%s", buff);
				fprintf(read,"%s", buff);
			}
			RemoveFIle("/home/pi/Desktop/mailBerry/fileMsg/messagerecvnoread.txt");
			if (clignote)
			{
				clignote = 0;
				pthread_cancel(thClignote);
				led("19", 0) ;
			}
			fclose(read);
		}
		fclose(noread);
	}

}

	

void Send(int fd, FILE * flowin, FILE * flowout, char * jaune,char * rouge)
{
	char recipient[100], message[1000], buf[1106], test[100];
	int nb_read;
	pthread_t th0 ;
	Msg msg;
	
	strcpy(buf, "send ");
	
	List(fd, flowout);
	
	fprintf(flowout, "a qui veux-tu envoyer le message?\n");
	fflush(flowin);
	fgets(recipient, 100, flowin);
	recipient[strlen(recipient) -1] = '\0'; //elimination de \n
	strcat(buf, recipient);
	strcat(buf, " ");
	
	fprintf(flowout, "quel est ton message?\n");
	fflush(flowin);
	fgets(message, 1000, flowin);
	message[strlen(message) -1] = '\0'; //elimination de \n
	strcat(buf, message);
	strcat(buf, "\n");
	printf("commande envoyee :%s", buf);
	write(fd, buf, strlen(buf));
	
	msgrcv(id, &msg, sizeof(Msg), 1, MSG_NOERROR);
	printf("code de message =%c\n",msg.code); 
	
	switch (msg.code)
	{
		case '0' :
		fprintf(flowout, "Message envoye et enregistre\n");
		SaveMessageSend(recipient, message);
		pthread_create (&th0 , NULL , &ledConfirmation , jaune);
		break;
		case '2' :
		case '5' :
		case '6' :
		case '7' :
		led(rouge, 1);
		fprintf(flowout, "erreur dans le destinataire ou le message\n");
		break;
		default :
		led(rouge, 1);
		fprintf(flowout, "erreur d'origine inconnue\n");
	}

}


void vider_queue_msg(int id) {
	int err=0;
	Msg msg;
	printf("On vide la queue de message :\n");
	while(err != -1) {
		err = msgrcv(id, &msg, sizeof(Msg), 1, IPC_NOWAIT |  MSG_NOERROR);
		printf("erreur %d\n", err);
		if(err!= -1)
			printf("%s", msg.code);
	}
	printf("\n");
}



void List(int fd, FILE * flowout)
{
	write(fd, "list\n", 5);
	usleep(10000);
	fprintf(flowout, "Les utilisateurs connectes sont :\n");
	ReadMessage("/home/pi/Desktop/mailBerry/fileMsg/list.txt", flowout);
}


void SaveList(char buf[100])
 {
	FILE * f = fopen("/home/pi/Desktop/mailBerry/fileMsg/list.txt", "w");
		if(f)
	{
		fprintf(f, "%s", buf);
		fclose(f);
	}
}


void erase(char * buf, int taille)
{
	int i;
	for (i=0;i<taille;i++)
	{
		buf[i]='\0';
	}
}


void * thread_recv (void * arg)
{
	int nb_read;
	char buf[100], r[100], msg[100], list[100];

	int fd = *(int*)arg;
	
	Msg ack;
	
	while(1)
	{
		erase(buf, 100);
		nb_read = read(fd, buf, 100);
		printf("réponse serveur %s", buf);
		switch (buf[0])
		{
			case 'a' :
			//rediriger vers une queue de message
			ack.mtype=1;
			ack.code=buf[4];
			msgsnd(id, &ack,  sizeof(Msg), IPC_NOWAIT);
			break;
			
			case 'r' :
			buf[nb_read -1] = '\0'; //elimination de \n
			SaveMessageRecv(buf);
			if(!  clignote)
			{
				clignote = 1 ;
				pthread_create (&thClignote , NULL , &thread_clignote , "19");
			}
			break;
			
			case 'u':
			//liste des utilisateurs connectes a mettre dans un fichier
			strcpy(list, buf);
			supressionElt(list, 6);
			SaveList(list);
			break;
			
			default :
			printf("je ne sais pas ce qu'il se passe, val = %d\n",nb_read);
		}
	}
}

