#ifndef SERVEUR_H
#define SERVEUR_H

int id;

typedef  struct {
	long  mtype;
	char  code;
} Msg;

void affichageNom(FILE* flowin, FILE * flowout, char * path, int type) ;
void affiche(FILE * flowout, char * name, char * path, int type) ;
void supressionElt(char * buff, int type) ;
int testName(char * name, char * buff, int type) ;
void login(int fd, FILE * flowin, FILE * flowout, char * jaune, char * rouge);
int tcp_connect(char *addr, char *port);
void RemoveFIle(char* path);
void Send(int fd, FILE * flowin, FILE * flowout, char * jaune,char * rouge);
void SaveMessageSend(char * recipient, char * msg);
void SaveMessageRecv(char * msg);
void ReadMessage(char * path, FILE * flowout);
void ReadAndSaveMessage(FILE * flowout);
void vider_queue_msg(int id) ;
void * thread_recv (void * arg);
void SaveList(char buf[100]);
void List(int fd, FILE * flowout);

#endif  
