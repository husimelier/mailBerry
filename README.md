# TP Linux embarqué utilisation de `mailBerry`
Manon LAGASSE | Hugo SIMELIERE

## Compilation coté Raspberry
Commande pour la compilation dans le dossier racine du projet:
```
make
```
De cette commande est créé l'exécutable `mailberry`.

C'est cet exécutable qui s'exécute avec le démon `mailberry.service` sur le Raspberry. 

## Compilation coté ordinateur
Commande pour la compilation dans le dossier `pc`:
```
make
```
De cette commande est créé l'exécutable `uart`.

## Fonctionnement:

Le programme coté ordinateur ne sert qu'à transmettre les informations saisies au clavier 
et d'afficher les informations envoyées par la Raspberry à l'écran.
Toute les opérations se font sur le Raspberry.

## Utilisation:
Lancer le programme `uart` (dans le dossier `pc`) coté ordinateur puis connecter le Raspberry.
Le programme demande à l'utilisateur de ce connecter en saisissant un identifiant. 

Une fois la connection établie un menu apparait, il permet à l'utilisateur:

* **D'envoyer un message** Demande de saisie du destinataire puis du message
* **De lire les messages envoyés** Aucune saisie clavier requise
* **De lire les messages envoyés par destinataire** Demande de saisie du destinataire
* **De lire les messages reçus déjà lus** Aucune saisie clavier requise
* **De lire les messages reçus déjà lus par emetteur** Demande de saisie de l'emetteur
* **De lire les messages reçus non lus** Aucune saisie clavier requise
* **De quitter l'application** Nécessite d'appuyer sur *entrer*

Pour choisir une des actions du menu il suffit de saisir au clavier le chiffre de '0' à '6' correspondant à l'action.

Lors du choix "Quitter l'application" le programme `uart` coté ordinateur s'arrête et le programme `mailberry` coté Raspberry s'arrête également. 
Ce dernier est ensuite réexécuté automatiquement au bout de 5 secondes par le démon `mailberry.service`.