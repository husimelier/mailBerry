#ifndef LED_H
#define LED_H

void led(char * gpio, int sens);
void init_led(char * gpio);
void * thread_clignote(void * ll);
void * ledConfirmation(void * ll);
void findeprog(char * jaune, char * verte, char * rouge);

#endif
